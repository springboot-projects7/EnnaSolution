package com.chandan.ennea.exception;

public class ProductNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private ErrorDetails errorResponse;

	public ProductNotFoundException() {
		super();
	}
	
	public ProductNotFoundException(String message) {
		super(message);
	}
	
	public ProductNotFoundException(Throwable cause) {
		super(cause);
	}

	public ProductNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}
	
	public ProductNotFoundException(ErrorDetails errorResponse) {
		this.errorResponse = errorResponse;
	}

	public ErrorDetails getErrorResponse() {
		return errorResponse;
	}

	public void setErrorResponse(ErrorDetails errorResponse) {
		this.errorResponse = errorResponse;
	}
}
