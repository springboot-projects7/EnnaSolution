package com.chandan.ennea.resource;

import static com.chandan.ennea.util.ExceptionUtils.handleException;
import static com.chandan.ennea.util.GenericUtils.basicMethodInfo;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.chandan.ennea.constants.MethodNames;
import com.chandan.ennea.entity.Product;
import com.chandan.ennea.service.ProductService;
import com.chandan.ennea.util.CsvToObjectConverter;
import com.chandan.ennea.util.FilterDto;
import com.chandan.ennea.util.LogUtils;
import com.chandan.ennea.util.ResponseMessage;

@RestController
@RequestMapping("/api/v1/product")
public class ProductController {
	@Autowired
	private ProductService prodService;

	@PostMapping("/upload")
	public ResponseEntity<ResponseMessage> saveFile(@RequestParam("file") MultipartFile file) {
		String message = "";

		if (CsvToObjectConverter.hasCSVFormat(file)) {
			try {
				LogUtils.debugLog.accept(
	                    basicMethodInfo.apply(getClass().getCanonicalName(), MethodNames.SAVE_FILE),
	                    String.format("Processing all product details "));
				prodService.save(file);

				message = "Uploaded the file successfully: " + file.getOriginalFilename();
				return ResponseEntity.status(HttpStatus.OK).body(new ResponseMessage(message));
			} catch (Exception e) {
				 handleException.accept(
		                    basicMethodInfo.apply(getClass().getCanonicalName(), MethodNames.SAVE_FILE), e,
		                    "Error occurred while saving product details data");
			}
		}

		message = "Please upload a csv file!";
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseMessage(message));
	}
	
	
	 @PostMapping("/list")
	    public Map<String, Object> getAllProducts(@RequestParam("pageNumber") Long pageNumber,
	                                                        @RequestParam("pageSize") Long pageSize) {
	        Map<String, Object> products = new HashMap<>();
	        try {
	            LogUtils.debugLog.accept(
	                    basicMethodInfo.apply(getClass().getCanonicalName(), MethodNames.GET_ALL_PRODUCT_DETAILS),
	                    String.format("Fetching all product details"));
	            Pageable pageable = PageRequest.of(pageNumber.intValue(), pageSize.intValue());
	            products = prodService.getAllProducts(pageable);
	            LogUtils.debugLog.accept(
	                    basicMethodInfo.apply(getClass().getCanonicalName(), MethodNames.GET_ALL_PRODUCT_DETAILS),
	                    String.format("Retrieved all product details"));
	        } catch (Exception e) {
	            handleException.accept(
	                    basicMethodInfo.apply(getClass().getCanonicalName(), MethodNames.GET_ALL_PRODUCT_DETAILS), e,
	                    "Error occurred while fetching product details");
	        }
	        return products;
	    }
	 
	 @GetMapping("/getByProductName")
	    public ResponseEntity<Product> getByProductName(@RequestParam("productName") String productName) {
	        Product product = new Product();
	        try {
	            LogUtils.debugLog.accept(
	                    basicMethodInfo.apply(getClass().getCanonicalName(), MethodNames.GET_PRODUCT_DETAILS_BY_NAME),
	                    String.format("Fetching  product details by product name %s", productName));
	            product = prodService.getByProductName(productName);
	            LogUtils.debugLog.accept(
	                    basicMethodInfo.apply(getClass().getCanonicalName(), MethodNames.GET_PRODUCT_DETAILS_BY_NAME),
	                    String.format("Retrieved product details by product name %s", productName));
	        } catch (Exception e) {
	            handleException.accept(
	                    basicMethodInfo.apply(getClass().getCanonicalName(), MethodNames.GET_PRODUCT_DETAILS_BY_NAME), e,
	                    "Error occurred while fetching requested product details");
	        }
	        return new ResponseEntity<Product>(product, HttpStatus.OK);
	    }
	 
	 @GetMapping("/getBySuppilersNameWithNotExpaired")
	    public Map<String, Object> getBySuppilersNameWithNotExpaired(@RequestBody FilterDto filterDto ,
																    		@RequestParam("pageNumber") Long pageNumber,
															                @RequestParam("pageSize") Long pageSize) {
		 Map<String, Object> products = new HashMap<>();
	        try {
	            LogUtils.debugLog.accept(
	                    basicMethodInfo.apply(getClass().getCanonicalName(), MethodNames.GET_BY_SUPPILERS_NAME_WITH_STOCKS),
	                    String.format("Fetching  product details by Suppilers name which has having stocks %s", filterDto.getSuppilerName()));
	            Pageable pageable = PageRequest.of(pageNumber.intValue(), pageSize.intValue());
	            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");  
	 		   	LocalDate now = LocalDate.now(); 
	            products = prodService.getBySuppliersWithNotExpaired(now, filterDto.getSuppilerName(), pageable);
	            LogUtils.debugLog.accept(
	                    basicMethodInfo.apply(getClass().getCanonicalName(), MethodNames.GET_BY_SUPPILERS_NAME_WITH_STOCKS),
	                    String.format("Retrieved product details by Suppilers name which has having stocks %s", filterDto.getSuppilerName()));
	        } catch (Exception e) {
	            handleException.accept(
	                    basicMethodInfo.apply(getClass().getCanonicalName(), MethodNames.GET_BY_SUPPILERS_NAME_WITH_STOCKS), e,
	                    "Error occurred while fetching requested product details");
	        }
	        return products;
	    }
	 
	 
	 @GetMapping("/getBySuppilersName")
	    public Map<String, Object> getBySuppilersNameWithStock(@RequestParam("suppilerName") String suppilerName,
																    		@RequestParam("pageNumber") Long pageNumber,
															                @RequestParam("pageSize") Long pageSize) {
		 Map<String, Object> products = new HashMap<>();
	        try {
	            LogUtils.debugLog.accept(
	                    basicMethodInfo.apply(getClass().getCanonicalName(), MethodNames.GET_BY_SUPPILERS_NAME_WITH_STOCKS),
	                    String.format("Fetching  product details by Suppilers name which has having stocks %s", suppilerName));
	            Pageable pageable = PageRequest.of(pageNumber.intValue(), pageSize.intValue());
	            products = prodService.getBySuppilerNameAndStockGreaterThanZero(suppilerName, 0, pageable);
	            LogUtils.debugLog.accept(
	                    basicMethodInfo.apply(getClass().getCanonicalName(), MethodNames.GET_BY_SUPPILERS_NAME_WITH_STOCKS),
	                    String.format("Retrieved product details by Suppilers name which has having stocks %s", suppilerName));
	        } catch (Exception e) {
	            handleException.accept(
	                    basicMethodInfo.apply(getClass().getCanonicalName(), MethodNames.GET_BY_SUPPILERS_NAME_WITH_STOCKS), e,
	                    "Error occurred while fetching requested product details");
	        }
	        return products;
	    }
	 
	 
}
