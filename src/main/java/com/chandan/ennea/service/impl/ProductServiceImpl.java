package com.chandan.ennea.service.impl;

import static com.chandan.ennea.util.ExceptionUtils.handleException;
import static com.chandan.ennea.util.GenericUtils.basicMethodInfo;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.chandan.ennea.constants.Generic;
import com.chandan.ennea.constants.MethodNames;
import com.chandan.ennea.entity.Product;
import com.chandan.ennea.repository.ProductRepository;
import com.chandan.ennea.service.ProductService;
import com.chandan.ennea.util.CsvToObjectConverter;

@Service
@Transactional
public class ProductServiceImpl implements ProductService {

	private final Logger log = LoggerFactory.getLogger(ProductServiceImpl.class);
	@Autowired
	private ProductRepository repository;

	@Override
	public void save(MultipartFile file) {
		try {
			List<Product> products = CsvToObjectConverter.csvToProduct(file);
			repository.saveAll(products);
		} catch(Exception e) {
			handleException.accept(
                    basicMethodInfo.apply(getClass().getCanonicalName(), MethodNames.SAVE_FILE), e,
                    "Error occurred while saving product details data");
		}
	}



	@Override
	public Map<String, Object> getAllProducts(Pageable pageable) {
		Map<String, Object> response = new LinkedHashMap<>();
		List<Product> productList = new ArrayList<>();
		log.debug("Getting Data of Products based on page");
		try {
			Page<Product> pageList = repository.findAll(pageable);
			productList = pageList.getContent();
			long total = repository.count();
			if (!productList.isEmpty()) {
				response.put("status", Generic.SUCCESS);
				response.put("message", Generic.FETCHED_DATA);
				response.put("data", new PageImpl<Product>(productList, pageable, total));
			} else {
				response.put("status", Generic.SUCCESS);
				response.put("message", Generic.NO_DATA);
				response.put("data", new PageImpl<Product>(productList));
			}

		} catch (Exception e) {
			handleException.accept(basicMethodInfo.apply(getClass().getCanonicalName(), MethodNames.GET_DATA_BY_PAGE),
					e, "Error occurred while fetching all product details details");

		}

		return response;
	}

	@Override
	public Product getByProductName(String prodName) {
		Product product = null;
		try {
			log.info("Retrieving data by Product name");
			product = new Product();
			product = repository.findAllByName(prodName);
			log.info("Retrieved data successfullly");
		} catch (Exception e) {
			handleException.accept(basicMethodInfo.apply(getClass().getCanonicalName(), MethodNames.GET_DATA_BY_PRODUCT_NAME),
					e, "Error occurred while fetching product details by product name");
		}
		return product;
	}

	@Override
	public Map<String, Object> getBySuppilerNameAndStockGreaterThanZero(String supplier, Integer stock, Pageable pageable) {
		Map<String, Object> response = new LinkedHashMap<>();
		List<Product> productList = new ArrayList<>();
		log.debug("Getting Data by suppiler name which has stocks");
		try {
			
			productList = repository.findBySuppilerAndStockGreaterThan(supplier, stock, pageable);
			if (!productList.isEmpty()) {
				response.put("status", Generic.SUCCESS);
				response.put("message", Generic.FETCHED_DATA);
				response.put("data", new PageImpl<Product>(productList));
			} else {
				response.put("status", Generic.SUCCESS);
				response.put("message", Generic.NO_DATA);
				response.put("data", new PageImpl<Product>(productList));
			}

		} catch (Exception e) {
			handleException.accept(basicMethodInfo.apply(getClass().getCanonicalName(), MethodNames.GET_DATA_BY_SUPPILERNAME),
					e, "Error occurred while fetching Data by suppiler name which has stocks");

		}
		return response;
	}

	@Override
	public Map<String, Object> getBySuppliersWithNotExpaired(LocalDate currentDate, List<String> suppiler, Pageable pageable) {
		Map<String, Object> response = new LinkedHashMap<>();
		List<Product> productList = new ArrayList<>();
		log.debug("Getting Data by suppiler names and not expired");
		try {
			productList = repository.findByExpGreaterThanCurrentDateAndSuppilerIn(currentDate, suppiler, pageable);
//			productList = repository.findByExpGreaterThanCurrentDateAndSuppilerIn(currentDate, suppiler);

			long total = productList.size();
			if (!productList.isEmpty()) {
				response.put("status", Generic.SUCCESS);
				response.put("message", Generic.FETCHED_DATA);
				response.put("data", new PageImpl<Product>(productList, pageable, total));
			} else {
				response.put("status", Generic.SUCCESS);
				response.put("message", Generic.NO_DATA);
				response.put("data", new PageImpl<Product>(productList));
			}

		} catch (Exception e) {
			handleException.accept(basicMethodInfo.apply(getClass().getCanonicalName(), MethodNames.GET_DATA_BY_SUPPILERNAME),
					e, "Error occurred while fetching Data by suppiler names and not expired");
		}
		return response;
	}




}
