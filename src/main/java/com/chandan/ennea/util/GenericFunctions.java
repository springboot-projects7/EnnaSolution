package com.chandan.ennea.util;
public class GenericFunctions {

	@FunctionalInterface
	public interface TriConsumer<T, U, R> {
		void accept(T t, U u, R r);
	}
	
	@FunctionalInterface
	public interface TetraConsumer<T, U, R, V> {
		void accept(T t, U u, R r,V v);
	}
	
	@FunctionalInterface
	public interface PentaConsumer<T, U, R, V, W> {
		void accept(T t, U u, R r, V v, W w);
	}
	
	@FunctionalInterface
	public interface TriPredicate<T, U, R> {
		boolean test(T t, U u, R r);
	}

	@FunctionalInterface
	public interface TriFunction<T, U, V, R> {
		R apply(T t, U u, V v);
	}

	@FunctionalInterface
	public interface TetraFunction<T, S, U, V, R> {
		R apply(T t, S s, U u, V v);
	}
	
	@FunctionalInterface
	public interface PentaFunction<T, S, U, V,W, R> {
		R apply(T t, S s, U u, V v, W w);
	}

	@FunctionalInterface
	public interface ThrowableConsumer<T, E extends Exception> {
		void accept(T t) throws E;
	}

	@FunctionalInterface
	public interface ThrowableBiConsumer<T, S, E extends Exception> {
		void accept(T t, S s) throws E;
	}

	@FunctionalInterface
	public interface ThrowableTriConsumer<P, T, S, E extends Exception> {
		void accept(P p, T t, S s) throws E;
	}

	@FunctionalInterface
	public interface ThrowableSupplier<T, E extends Exception> {
		T get() throws E;
	}

	@FunctionalInterface
	public interface ThrowableFunction<T, R, E extends Exception> {
		R apply(T t) throws E;
	}

	@FunctionalInterface
	public interface ThrowablePredicate<T, E extends Exception> {
		boolean test(T t) throws E;
	}

	@FunctionalInterface
	public interface ThrowableBiFunction<T, S, R, E extends Exception> {
		R apply(T t, S s) throws E;
	}

	@FunctionalInterface
	public interface ThrowableTriFunction<T, S, V, R, E extends Exception> {
		R apply(T t, S s, V v) throws E;
	}

}
