package com.chandan.ennea.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.chandan.ennea.entity.Product;
import com.opencsv.CSVReader;

public class CsvToObjectConverter {

	public static String TYPE = "text/csv";

	public static boolean hasCSVFormat(MultipartFile file) {

		if (!TYPE.equals(file.getContentType())) {
			return false;
		}

		return true;
	}

	public static List<Product> csvToProduct(MultipartFile file) {
		List<Product> prodList = new ArrayList<Product>();
        try  {
        	File f1 = convert(file);
        	CSVReader reader = new CSVReader(new FileReader(f1));
            List<String[]> r = reader.readAll();
            r.remove(0);
            
            for(String[] str: r) {
            	Product product = new Product();
            	product.setCode(str[0] != null ? str[0].toString() : null);
            	product.setName(str[1] != null ? str[1].toString() : null);
            	product.setBatch(str[2] != null ? str[2].toString() : null);
            	product.setStock(str[3] != null ? Integer.parseInt(str[3].toString()) : null);
            	product.setDeal(str[4] != null ? Integer.parseInt(str[4].toString()) : null);
            	product.setFree(str[5] != null ? Integer.parseInt(str[5].toString()) : null);
            	product.setMrp(str[6] != null ? Double.parseDouble(str[6].toString()) : null);
            	product.setRate(str[7] != null ? Double.parseDouble(str[7].toString()) : null);
            	product.setExp((str[8] != null && !str[8].isEmpty()) ? Date.valueOf(str[8].toString()).toLocalDate() : null);
            	product.setCompany(str[9] != null ? str[9].toString() : null);
            	product.setSuppiler(str[10] != null ? str[10].toString() : null);
            	
            	prodList.add(product);
            }
            
            
            
        } catch (Exception e) {
        	System.out.println(e.getMessage());
        	System.out.println();
        	System.out.println(e.toString());
		}
        return prodList;
    }

	public static File convert(MultipartFile file) throws IOException {
		File convFile = new File(file.getOriginalFilename());
		convFile.createNewFile();
		FileOutputStream fos = new FileOutputStream(convFile);
		fos.write(file.getBytes());
		fos.close();
		return convFile;
	}

}
