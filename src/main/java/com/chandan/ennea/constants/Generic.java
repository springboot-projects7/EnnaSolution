package com.chandan.ennea.constants;

public class Generic {

	public static final String BASE_PATH = "/PRODUCT/";
	public static final String SUCCESS = "Success";
    public static final String FAILURE = "Failure";
    public static final String FETCHED_DATA = "Retrieved Data Successfully!";
    public static final String NO_DATA = "No Records Found";
}
